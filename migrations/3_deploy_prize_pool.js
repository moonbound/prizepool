const BigNumber = web3.BigNumber;

const ZapCoordinator = artifacts.require('./ZapCoordinator.sol');
const ZapToken = artifacts.require("./ZapToken.sol");
const TokenAdapter = artifacts.require('./TokenAdapter.sol');
const TokenFactory = artifacts.require('./TokenFactory.sol');
const PrizePool = artifacts.require('./PrizePool.sol');

const reserveTokens = new BigNumber("1500e18");

const deploy = async function(deployer, network) {
    console.log("Deploying main contracts on: " + network);

    const coordInstance = await ZapCoordinator.deployed();

    // token used internally for bonding, unbonding
    let reserveToken = await ZapToken.deployed();

    // Deploy token factory
    let tokenFactory = await deployer.deploy(TokenFactory);

    // Deploy bounty pool  
    let bountyPool = await deployer.deploy(PrizePool, coordInstance.address, TokenFactory.address);

    // Deploy gated market    
    let gatedMarket = await deployer.deploy(TokenAdapter, coordInstance.address, TokenFactory.address, ZapToken.address);

    // Set rate of internal reserve currency -> gated market token exchange rate
    await gatedMarket.setAdapterRate(1);

    // Set gated market to be operated by PrizePool
    await gatedMarket.transferOwnership(PrizePool.address);

    // Set Boounty Pool to use gated market    
    await bountyPool.setMarket(gatedMarket.address);

    // Allocate reserve token to PrizePool
    await reserveToken.allocate(bountyPool.address, reserveTokens)

    // Allocate reserve token to gated market
    await reserveToken.allocate(gatedMarket.address, reserveTokens)

}

module.exports = (deployer, network) => {
    deployer.then(async () => await deploy(deployer, network));
};
