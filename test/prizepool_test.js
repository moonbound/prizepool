import EVMRevert from './helpers/EVMRevert';

const BigNumber = web3.BigNumber;

const expect = require('chai')
    .use(require('chai-as-promised'))
    .use(require('chai-bignumber')(BigNumber))
    .expect;

const Utils = require('./helpers/utils');

const ZapCoordinator = artifacts.require("ZapCoordinator");
const Database = artifacts.require("Database");
const Bondage = artifacts.require("Bondage");
const Registry = artifacts.require("Registry");
const ZapToken = artifacts.require("ZapToken");
const Cost = artifacts.require("CurrentCost");
const TokenAdapter = artifacts.require("TokenAdapter");
const FactoryToken = artifacts.require("FactoryToken");
const TokenFactory = artifacts.require("TokenFactory");
const PrizePool = artifacts.require("PrizePool");

contract('PrizePool', function (accounts) {
    const owner = accounts[0];
    const user = accounts[1];
    const oracle = accounts[2];
    const factoryOwner = accounts[3];

    const publicKey = 111;
    const marketPublicKey = 222;
    const title = "test";
    const marketTitle = "test_1";
    const routeKeys = [1];
    const params = ["param1", "param2"];

    const specifier = "test-specifier";
    const marketSpecifier = "test_spec_1";
    const zeroAddress = Utils.ZeroAddress;

    const piecewiseFunction = [3, 0, 0, 2, 10000];
    const broker = 0;

    const tokensForOwner = new BigNumber("1500e18");
    const userTokenApprovalLimit = new BigNumber("5000e18");
    const approveTokens = new BigNumber("1000e18");
    const dotBound = new BigNumber("999");

    async function prepareTokens(allocAddress = user) {
        await this.token.allocate(owner, tokensForOwner, { from: owner });
        await this.token.allocate(allocAddress, userTokenApprovalLimit, { from: owner });
        await this.token.approve(this.bondage.address, approveTokens, {from: user});
    }

    beforeEach(async function deployContracts() {

        ///Zap contracts bootstrapping

        // Deploy initial contracts
        this.currentTest.token = await ZapToken.new();
        this.currentTest.coord = await ZapCoordinator.new();
        const owner = await this.currentTest.coord.owner();
        this.currentTest.db = await Database.new();
        await this.currentTest.db.transferOwnership(this.currentTest.coord.address);
        await this.currentTest.coord.addImmutableContract('DATABASE', this.currentTest.db.address);
        await this.currentTest.coord.addImmutableContract('ZAP_TOKEN', this.currentTest.token.address);

        // Deploy registry
        this.currentTest.registry = await Registry.new(this.currentTest.coord.address);
        await this.currentTest.coord.updateContract('REGISTRY', this.currentTest.registry.address);

        // Deploy current cost
        this.currentTest.cost = await Cost.new(this.currentTest.coord.address);
        await this.currentTest.coord.updateContract('CURRENT_COST', this.currentTest.cost.address);

        // Deploy Bondage
        this.currentTest.bondage = await Bondage.new(this.currentTest.coord.address);
        await this.currentTest.coord.updateContract('BONDAGE', this.currentTest.bondage.address);

        // Hack for making arbiter an account we control for testing the escrow
        await this.currentTest.coord.addImmutableContract('ARBITER', accounts[3]);

        await this.currentTest.coord.updateAllDependencies({ from: owner });

        this.currentTest.tokenFactory = await TokenFactory.new();
    });

    it("BOOLEAN_POOL_1 - status=Uninitialized, initialize new gateway bonding curve", async function () {

        let factory = await PrizePool.new(this.test.coord.address, this.test.tokenFactory.address, {from: factoryOwner});
        await factory.initializeGateway(title, publicKey, specifier, 'a', piecewiseFunction, 1, {from: factoryOwner});

    });

    it("BOOLEAN_POOL_2 - status=Initialized, gateway bond with eth", async function () {

        let factory = await PrizePool.new(this.test.coord.address, this.test.tokenFactory.address, {from: factoryOwner});

        //allocate reserve tokens for internal bonding in gateway 
        await prepareTokens.call(this.test, factory.address);

        //initialize new gateway
        await factory.initializeGateway(title, publicKey, specifier, 'a', piecewiseFunction, 1, {from: factoryOwner});

        let gatewayBondPrice = await factory.getAdapterPrice(specifier, 18);
        // console.log('bond price for 18: ', gatewayBondPrice.toNumber());

        //user bond to gateway
        await factory.gatewayBond(18, {from: user, value: 4218});

    });

    it("BOOLEAN_POOL_3 - status=Initialized, check that gateway owner can toggle gateway bond/unbond", async function () {

        let factory = await PrizePool.new(this.test.coord.address, this.test.tokenFactory.address, {from: factoryOwner});

        //allocate reserve tokens for internal bonding in gateway 
        await prepareTokens.call(this.test, factory.address);

        //initialize new gateway
        await factory.initializeGateway(title, publicKey, specifier, 'a', piecewiseFunction, 1, {from: factoryOwner});

        let gatewayBondPrice = await factory.getAdapterPrice(specifier, 18);
        // console.log('bond price for 18: ', gatewayBondPrice.toNumber());

        //user bond to gateway
        await factory.gatewayBond(9, {from: user, value: 4218});

        //owner disallow bond 
        await factory.allowBond(false, {from: factoryOwner});

        //user bond to gateway
        await expect(factory.gatewayBond(9, {from: user, value: 4218})).to.be.eventually.rejectedWith(EVMRevert);
    });

    it("BOOLEAN_POOL_4 - status=Initialized, initialize new candidate bonding curve", async function () {

        let factory = await PrizePool.new(this.test.coord.address, this.test.tokenFactory.address, {from: factoryOwner});

        //allocate reserve tokens for internal bonding in gateway 
        await prepareTokens.call(this.test, factory.address);

        //initialize new gateway
        await factory.initializeGateway(title, publicKey, specifier, 'a', piecewiseFunction, 1, {from: factoryOwner});

        let gatewayTokenAddress = await factory.gatewayToken();

        let gatewayBondPrice = await factory.getAdapterPrice(specifier, 18);
        // console.log('bond price for 18: ', gatewayBondPrice.toNumber());

        //user bond to gateway
        await factory.gatewayBond(18, {from: user, value: 4218});

        //create gated market for candidate curves
        let market = await TokenAdapter.new(this.test.coord.address, this.test.tokenFactory.address, gatewayTokenAddress, {from: factoryOwner});
        //bootstrap gated market and gateway
        await market.setAdapterRate(1, {from: factoryOwner});
        await market.transferOwnership(factory.address, {from: factoryOwner});
        await factory.setMarket(market.address, {from: factoryOwner});

        //load gated market with reserve tokens of internal bonding 
        await this.test.token.allocate(market.address, userTokenApprovalLimit);

        //initialize gated contest  
        await factory.initializeCompetition("Big Comp", oracle, {from: factoryOwner}); 

        let gatewayToken = await FactoryToken.at(gatewayTokenAddress);
        await gatewayToken.approve(factory.address, userTokenApprovalLimit, {from: user});
        await gatewayToken.approve(market.address, userTokenApprovalLimit, {from: user});

        let userBalance = await gatewayToken.balanceOf(user);
        console.log('gateway balance user: ', userBalance.toNumber());

        //initialize candidate bonding curve
        await factory.initializeMarketCurve(marketPublicKey, marketTitle, marketSpecifier, 'b', piecewiseFunction, {from: user});
        let candidateBondPrice = await market.getAdapterPrice(marketSpecifier, 1);
        console.log('candidate bond price for 1: ', candidateBondPrice.toNumber());

    });

    it("BOOLEAN_POOL_5 - status=Initialized, candidate bond: gateway token to candidate token", async function () {

        let factory = await PrizePool.new(this.test.coord.address, this.test.tokenFactory.address, {from: factoryOwner});

        //allocate reserve tokens for internal bonding in gateway 
        await prepareTokens.call(this.test, factory.address);

        //initialize new gateway
        await factory.initializeGateway(title, publicKey, specifier, 'a', piecewiseFunction, 1, {from: factoryOwner});

        let gatewayTokenAddress = await factory.gatewayToken();

        let gatewayBondPrice = await factory.getAdapterPrice(specifier, 18);
        // console.log('bond price for 18: ', gatewayBondPrice.toNumber());

        //user bond to gateway
        await factory.gatewayBond(18, {from: user, value: 4218});

        //create gated market for candidate curves
        let market = await TokenAdapter.new(this.test.coord.address, this.test.tokenFactory.address, gatewayTokenAddress, {from: factoryOwner});
        //bootstrap gated market and gateway
        await market.setAdapterRate(1, {from: factoryOwner});
        await market.transferOwnership(factory.address, {from: factoryOwner});
        await factory.setMarket(market.address, {from: factoryOwner});

        //load gated market with reserve tokens of internal bonding 
        await this.test.token.allocate(market.address, userTokenApprovalLimit);

        //initialize gated contest  
        await factory.initializeCompetition("Big Comp", oracle, {from: factoryOwner}); 

        let gatewayToken = await FactoryToken.at(gatewayTokenAddress);
        await gatewayToken.approve(factory.address, userTokenApprovalLimit, {from: user});
        await gatewayToken.approve(market.address, userTokenApprovalLimit, {from: user});

        let userBalance = await gatewayToken.balanceOf(user);
        console.log('gateway balance user: ', userBalance.toNumber());

        //initialize candidate bonding curve
        await factory.initializeMarketCurve(marketPublicKey, marketTitle, marketSpecifier, 'b', piecewiseFunction, {from: user});
        let candidateBondPrice = await market.getAdapterPrice(marketSpecifier, 1);
        console.log('candidate bond price for 1: ', candidateBondPrice.toNumber());

        //bond to a candidate
        await factory.marketBond(marketSpecifier, 2, {from: user});



    });

    it("BOOLEAN_POOL_6 - status=Initialized, candidate unbond: candidate token to gateway token", async function () {

        let factory = await PrizePool.new(this.test.coord.address, this.test.tokenFactory.address, {from: factoryOwner});

        //allocate reserve tokens for internal bonding in gateway 
        await prepareTokens.call(this.test, factory.address);

        //initialize new gateway
        await factory.initializeGateway(title, publicKey, specifier, 'a', piecewiseFunction, 1, {from: factoryOwner});

        let gatewayTokenAddress = await factory.gatewayToken();

        let gatewayBondPrice = await factory.getAdapterPrice(specifier, 18);
        // console.log('bond price for 18: ', gatewayBondPrice.toNumber());

        //user bond to gateway
        await factory.gatewayBond(18, {from: user, value: 4218});

        //create gated market for candidate curves
        let market = await TokenAdapter.new(this.test.coord.address, this.test.tokenFactory.address, gatewayTokenAddress, {from: factoryOwner});
        //bootstrap gated market and gateway
        await market.setAdapterRate(1, {from: factoryOwner});
        await market.transferOwnership(factory.address, {from: factoryOwner});
        await factory.setMarket(market.address, {from: factoryOwner});

        //load gated market with reserve tokens of internal bonding 
        await this.test.token.allocate(market.address, userTokenApprovalLimit);

        //initialize gated contest  
        await factory.initializeCompetition("Big Comp", oracle, {from: factoryOwner}); 

        let gatewayToken = await FactoryToken.at(gatewayTokenAddress);
        await gatewayToken.approve(factory.address, userTokenApprovalLimit, {from: user});
        await gatewayToken.approve(market.address, userTokenApprovalLimit, {from: user});

        let userBalance = await gatewayToken.balanceOf(user);
        console.log('gateway balance user: ', userBalance.toNumber());

        //initialize candidate bonding curve
        await factory.initializeMarketCurve(marketPublicKey, marketTitle, marketSpecifier, 'b', piecewiseFunction, {from: user});

        let candTokenAddress  = await factory.getCandidateToken(marketSpecifier);

        let candToken = await ZapToken.at(candTokenAddress);

        await candToken.approve(market.address, userTokenApprovalLimit, {from: user});

        let candidateBondPrice = await market.getAdapterPrice(marketSpecifier, 1);
        console.log('candidate bond price for 1: ', candidateBondPrice.toNumber());

        //bond to a candidate
        await factory.marketBond(marketSpecifier, 2, {from: user});
        
        let userCandBalance = await factory.getCandidateBalance(marketSpecifier, {from:user});
        console.log('user cand bal: ', userCandBalance.toNumber());
        
        //unbond from candidate
        await factory.marketUnbond(marketSpecifier, 2, {from: user});
       
        userCandBalance = await factory.getCandidateBalance(marketSpecifier, {from:user});
        console.log('user cand bal: ', userCandBalance.toNumber());


    });

    it("BOOLEAN_POOL_7 - status=Initialized, 'close': owner close contest", async function () {

        let factory = await PrizePool.new(this.test.coord.address, this.test.tokenFactory.address, {from: factoryOwner});
        
        //allocate reserve tokens for internal bonding in gateway 
        await prepareTokens.call(this.test, factory.address);

        //initialize new gateway
        await factory.initializeGateway(title, publicKey, specifier, 'a', piecewiseFunction, 1, {from: factoryOwner});

        let gatewayTokenAddress = await factory.gatewayToken();
        let status = await factory.getStatus();
        console.log(status.toNumber());

        let gatewayBondPrice = await factory.getAdapterPrice(specifier, 18);
        // console.log('bond price for 18: ', gatewayBondPrice.toNumber());

        //user bond to gateway
        await factory.gatewayBond(18, {from: user, value: 4218});
 
        //create gated market for candidate curves
        let market = await TokenAdapter.new(this.test.coord.address, this.test.tokenFactory.address, gatewayTokenAddress, {from: factoryOwner});
        //bootstrap gated market and gateway
        await market.setAdapterRate(1, {from: factoryOwner});
        await market.transferOwnership(factory.address, {from: factoryOwner});
        await factory.setMarket(market.address, {from: factoryOwner});

        //load gated market with reserve tokens of internal bonding 
        await this.test.token.allocate(market.address, userTokenApprovalLimit);

        //initialize gated contest  
        await factory.initializeCompetition("Big Comp", oracle, {from: factoryOwner}); 

        let gatewayToken = await FactoryToken.at(gatewayTokenAddress);
        await gatewayToken.approve(factory.address, userTokenApprovalLimit, {from: user});
        await gatewayToken.approve(market.address, userTokenApprovalLimit, {from: user});

        let userBalance = await gatewayToken.balanceOf(user);
        console.log('gateway balance user: ', userBalance.toNumber());

        //initialize candidate bonding curve
        await factory.initializeMarketCurve(marketPublicKey, marketTitle, marketSpecifier, 'b', piecewiseFunction, {from: user});
        let candidateBondPrice = await market.getAdapterPrice(marketSpecifier, 1);
        console.log('candidate bond price for 1: ', candidateBondPrice.toNumber());

        //bond to a candidate
        await factory.marketBond(marketSpecifier, 2, {from: user});

        //close contest
        await factory.close({from: factoryOwner});  
        status = await factory.getStatus();
        console.log(status.toNumber());

        let candidates = await factory.getCandidates();
        let oracleTest = await factory.getOracle();
        console.log('oracle test: ', oracleTest);


    });

    it("BOOLEAN_POOL_8 - status=ReadyToSettle, 'judge': owner close contest", async function () {

        let factory = await PrizePool.new(this.test.coord.address, this.test.tokenFactory.address, {from: factoryOwner});
        
        //allocate reserve tokens for internal bonding in gateway 
        await prepareTokens.call(this.test, factory.address);

        //initialize new gateway
        await factory.initializeGateway(title, publicKey, specifier, 'a', piecewiseFunction, 1, {from: factoryOwner});

        let gatewayTokenAddress = await factory.gatewayToken();
        let status = await factory.getStatus();
        console.log(status.toNumber());

        let gatewayBondPrice = await factory.getAdapterPrice(specifier, 18);
        // console.log('bond price for 18: ', gatewayBondPrice.toNumber());

        //user bond to gateway
        await factory.gatewayBond(18, {from: user, value: 4218});
 
        //create gated market for candidate curves
        let market = await TokenAdapter.new(this.test.coord.address, this.test.tokenFactory.address, gatewayTokenAddress, {from: factoryOwner});
        //bootstrap gated market and gateway
        await market.setAdapterRate(1, {from: factoryOwner});
        await market.transferOwnership(factory.address, {from: factoryOwner});
        await factory.setMarket(market.address, {from: factoryOwner});

        //load gated market with reserve tokens of internal bonding 
        await this.test.token.allocate(market.address, userTokenApprovalLimit);

        //initialize gated contest  
        await factory.initializeCompetition("Big Comp", oracle, {from: factoryOwner}); 

        let gatewayToken = await FactoryToken.at(gatewayTokenAddress);
        await gatewayToken.approve(factory.address, userTokenApprovalLimit, {from: user});
        await gatewayToken.approve(market.address, userTokenApprovalLimit, {from: user});

        let userBalance = await gatewayToken.balanceOf(user);
        console.log('gateway balance user: ', userBalance.toNumber());

        //initialize candidate bonding curve
        await factory.initializeMarketCurve(marketPublicKey, marketTitle, marketSpecifier, 'b', piecewiseFunction, {from: user});
        let candidateBondPrice = await market.getAdapterPrice(marketSpecifier, 1);
        console.log('candidate bond price for 1: ', candidateBondPrice.toNumber());

        //bond to a candidate
        await factory.marketBond(marketSpecifier, 2, {from: user});

        //close contest
        await factory.close({from: factoryOwner});  
        status = await factory.getStatus();
        console.log(status.toNumber());

        let candidates = await factory.getCandidates();
        let oracleTest = await factory.getOracle();
        console.log('oracle test: ', oracleTest);

        console.log(candidates);

        //judge
        await factory.judge(candidates[0],{from: oracle});  
        status = await factory.getStatus();
        console.log(status.toNumber());

        let winner = await factory.getWinner();
        console.log('winner: ', winner);

        let issuance = await factory.getWinnerIssuance(); 
        console.log('issued: ', issuance.toNumber());

        let ethBonded = await web3.eth.getBalance(factory.address);
        console.log('eth bonded: ', ethBonded.toNumber());

    });

    it("BOOLEAN_POOL_9 - status=Judged, 'settle': calc winner-token:ETH rate", async function () {

        let factory = await PrizePool.new(this.test.coord.address, this.test.tokenFactory.address, {from: factoryOwner});
        
        //allocate reserve tokens for internal bonding in gateway 
        await prepareTokens.call(this.test, factory.address);

        //initialize new gateway
        await factory.initializeGateway(title, publicKey, specifier, 'a', piecewiseFunction, 1, {from: factoryOwner});

        let gatewayTokenAddress = await factory.gatewayToken();
        let status = await factory.getStatus();
        console.log(status.toNumber());

        let gatewayBondPrice = await factory.getAdapterPrice(specifier, 18);
        // console.log('bond price for 18: ', gatewayBondPrice.toNumber());

        //user bond to gateway
        await factory.gatewayBond(18, {from: user, value: 4218});
 
        //create gated market for candidate curves
        let market = await TokenAdapter.new(this.test.coord.address, this.test.tokenFactory.address, gatewayTokenAddress, {from: factoryOwner});
        //bootstrap gated market and gateway
        await market.setAdapterRate(1, {from: factoryOwner});
        await market.transferOwnership(factory.address, {from: factoryOwner});
        await factory.setMarket(market.address, {from: factoryOwner});

        //load gated market with reserve tokens of internal bonding 
        await this.test.token.allocate(market.address, userTokenApprovalLimit);

        //initialize gated contest  
        await factory.initializeCompetition("Big Comp", oracle, {from: factoryOwner}); 

        let gatewayToken = await FactoryToken.at(gatewayTokenAddress);
        await gatewayToken.approve(factory.address, userTokenApprovalLimit, {from: user});
        await gatewayToken.approve(market.address, userTokenApprovalLimit, {from: user});

        let userBalance = await gatewayToken.balanceOf(user);
        console.log('gateway balance user: ', userBalance.toNumber());

        //initialize candidate bonding curve
        await factory.initializeMarketCurve(marketPublicKey, marketTitle, marketSpecifier, 'b', piecewiseFunction, {from: user});
        let candidateBondPrice = await market.getAdapterPrice(marketSpecifier, 1);
        console.log('candidate bond price for 1: ', candidateBondPrice.toNumber());

        //bond to a candidate
        await factory.marketBond(marketSpecifier, 2, {from: user});

        //close contest
        await factory.close({from: factoryOwner});  
        status = await factory.getStatus();
        console.log(status.toNumber());

        let candidates = await factory.getCandidates();
        let oracleTest = await factory.getOracle();
        console.log('oracle test: ', oracleTest);

        console.log(candidates);

        //judge
        await factory.judge(candidates[0],{from: oracle});  
        status = await factory.getStatus();
        console.log(status.toNumber());

        let winner = await factory.getWinner();
        console.log('winner: ', winner);

        let issuance = await factory.getWinnerIssuance(); 
        console.log('issued: ', issuance.toNumber());

        let ethBonded = await web3.eth.getBalance(factory.address);
        console.log('eth bonded: ', ethBonded.toNumber());

        //settle win val        
        await factory.settle(); 
        status = await factory.getStatus();
        console.log(status.toNumber());

        let winVal = await factory.getWinValue();
        console.log('winVal: ', winVal.toNumber());

    });

    it("BOOLEAN_POOL_10 - status=Settled,  'redeem': winnerToken holder redeems for eth at winner-token:ETH rate", async function () {

        let factory = await PrizePool.new(this.test.coord.address, this.test.tokenFactory.address, {from: factoryOwner});
        
        //allocate reserve tokens for internal bonding in gateway 
        await prepareTokens.call(this.test, factory.address);

        //initialize new gateway
        await factory.initializeGateway(title, publicKey, specifier, 'a', piecewiseFunction, 1, {from: factoryOwner});

        let gatewayTokenAddress = await factory.gatewayToken();
        let status = await factory.getStatus();
        console.log(status.toNumber());

        let gatewayBondPrice = await factory.getAdapterPrice(specifier, 18);
        // console.log('bond price for 18: ', gatewayBondPrice.toNumber());

        //user bond to gateway
        await factory.gatewayBond(18, {from: user, value: 4218});
 
        //create gated market for candidate curves
        let market = await TokenAdapter.new(this.test.coord.address, this.test.tokenFactory.address, gatewayTokenAddress, {from: factoryOwner});
        //bootstrap gated market and gateway
        await market.setAdapterRate(1, {from: factoryOwner});
        await market.transferOwnership(factory.address, {from: factoryOwner});
        await factory.setMarket(market.address, {from: factoryOwner});

        //load gated market with reserve tokens of internal bonding 
        await this.test.token.allocate(market.address, userTokenApprovalLimit);

        //initialize gated contest  
        await factory.initializeCompetition("Big Comp", oracle, {from: factoryOwner}); 

        let gatewayToken = await FactoryToken.at(gatewayTokenAddress);
        await gatewayToken.approve(factory.address, userTokenApprovalLimit, {from: user});
        await gatewayToken.approve(market.address, userTokenApprovalLimit, {from: user});

        let userBalance = await gatewayToken.balanceOf(user);
        console.log('gateway balance user: ', userBalance.toNumber());

        //initialize candidate bonding curve
        await factory.initializeMarketCurve(marketPublicKey, marketTitle, marketSpecifier, 'b', piecewiseFunction, {from: user});
        await factory.initializeMarketCurve(marketPublicKey, marketTitle, '1', 'b', piecewiseFunction, {from: factoryOwner});
        await factory.initializeMarketCurve(marketPublicKey, marketTitle, '2', 'b', piecewiseFunction, {from: factoryOwner});
        await factory.initializeMarketCurve(marketPublicKey, marketTitle, '3', 'b', piecewiseFunction, {from: factoryOwner});
        await factory.initializeMarketCurve(marketPublicKey, marketTitle, '4', 'b', piecewiseFunction, {from: factoryOwner});

        let candidateBondPrice = await market.getAdapterPrice(marketSpecifier, 1);
        console.log('candidate bond price for 1: ', candidateBondPrice.toNumber());

        //bond to a candidate
        await factory.marketBond(marketSpecifier, 2, {from: user});

        //close contest
        await factory.close({from: factoryOwner});  
        status = await factory.getStatus();
        console.log(status.toNumber());

        let candidates = await factory.getCandidates();
        let oracleTest = await factory.getOracle();
        console.log('oracle test: ', oracleTest);

        console.log(candidates);

        //judge
        await factory.judge(candidates[0],{from: oracle});  
        status = await factory.getStatus();
        console.log(status.toNumber());

        let winner = await factory.getWinner();
        console.log('winner: ', winner);

        let issuance = await factory.getWinnerIssuance(); 
        console.log('issued: ', issuance.toNumber());

        let ethBonded = await web3.eth.getBalance(factory.address);
        console.log('eth bonded: ', ethBonded.toNumber());

        //settle win val        
        await factory.settle(); 
        status = await factory.getStatus();
        console.log(status.toNumber());

        let winVal = await factory.getWinValue();
        console.log('winVal: ', winVal.toNumber());

        await factory.redeem({from: user}); 

        ethBonded = await web3.eth.getBalance(factory.address);
        console.log('eth bonded: ', ethBonded.toNumber());               

    });
});
