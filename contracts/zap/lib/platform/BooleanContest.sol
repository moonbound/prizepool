import "./EthAdapter.sol";

/*
Contest is created with an array of position labels,
these are used as curve specifiers which mint tokens representing stake in a contest candidate
the array of specifiers will also be used to settle contest. oracle will supply a boolean value for each specifier representing winning/losing
owner can set status of contest. While contest status == Initialized, users can buy and redeem their stake in candidates by bonding/redeeming their dot-backed candidate tokens
while status == ReadyToSettle, oracle supplies boolean array corresponding to candidate curve specifiers
allocateWinnings calculates a value for winning tokens
redeemPosition allows winning token holders to redeem their proportional share of winnings
*/

contract BooleanContest is EthAdapter{

    enum ContestStatus { 
        Uninitialized,    // oracle, curves unset 
        Initialized,      // ready for buys
        ReadyToSettle,    // ready for oracle settlement
        Settled           // contest settled
    }

    struct Position{
        address token;    // dot-backed token representing position
        bytes32 specifier;// label of dot backed position
        uint256 quantity; // overall dots bonded
        bool alive;       // is this position alive ( todo: parent contest class makes use)
        mapping (address => uint256) spentWei; // how much wei user spend
    }

    bytes32 title;        // title of competition
    address oracle;       // address to end and settle competition
    bytes32[] positionLabels;// position labels

    Position[] public positions;
    ContestStatus public status;
    
    event PositionCreated(
        address indexed token,
        bytes32 indexed specifier
    );

    event PositionTaken(
        address indexed token,
        bytes32 indexed specifier,
        uint quantity
    );

    event Redeemed(
        address _sender,
        uint256 _value
    );

    constructor(address coordinator, address tokenFactory, uint256 rate) 
    EthAdapter(coordinator, tokenFactory, rate) {

        status = ContestStatus.Uninitialized;
    } 

    ///intialize position dot(curve) backed token 
    function initializeContest(
        bytes32 _title,
        uint256 pubKey,
        bytes32[] labels,
        int256[] curve,
        address _oracle 
    ) public onlyOwner {
        
        require(status ==  ContestStatus.Uninitialized, 
            "contest already initialized");

        title = _title;
        oracle = _oracle;
    
        positionLabels = labels;

        for( uint256 i = 0; i < labels.length; i++) {
            
            //concatenate title with each position for token name    
            bytes32[] memory pos = new bytes32[](2);
            pos[0] = title;
            pos[1] = labels[i];
            string memory name = bytes32ArrayToString(pos);

            positions.push( 
               Position( 
                    initializeCurve(
                        pubKey, title, stringToBytes32(name), labels[i], curve
                    ),
                    stringToBytes32(name), 0, true
               )
            );                                    
            PositionCreated(positions[i].token, positions[i].specifier);
        }
       status = ContestStatus.Initialized; 
    }

    ///purchase a position token with eth
    function buyPosition(uint256 index, uint256 quantity) public payable {

        require(status == ContestStatus.Initialized);
        super.bond(msg.sender, positions[index].specifier, quantity);
        positions[index].quantity += quantity;
        positions[index].spentWei[msg.sender] += msg.value;
    }  

    ///redeem position token after contest is closed
    function redeemPosition(uint256 index, uint256 quantity) public {

        require(status == ContestStatus.Settled);

        FactoryTokenInterface tok = FactoryTokenInterface(positions[index].token);
        tok.burnFrom(msg.sender, quantity);

        if (positions[index].alive) {
            msg.sender.send(positions[index].spentWei[msg.sender]);
        }

        emit Redeemed(msg.sender, positions[index].spentWei[msg.sender]);
    }

    ///oracle settlement function. true, false responses for each label in positionLabels
    function settle(bool[] response) {
        
        require(msg.sender == oracle, "wrong oracle");
        require(status == ContestStatus.ReadyToSettle, "not closed");

        status = ContestStatus.Settled;
        allocateWinners(response);
    }

    ///called to reallocate eth reserves
    function allocateWinners(bool[] response) {
        
        uint256 dead;
        uint256 rewards;
        uint256 endPrice;

        bondage = BondageInterface(coord.getContract("BONDAGE")); 

        for( uint256 i = 0; i < response.length; i++ ){
            positions[i].alive = response[i] ? false : true;
            bondage.unbond(address(this), positions[i].specifier, positions[i].quantity);
            if(!positions[i].alive) {
                dead++;
            }
        }

        require(positions.length > dead, "no winners");
    }

    function setStatus(uint statusIndex) onlyOwner {
        status = ContestStatus(statusIndex);
    }

    function getPosition(uint index) returns( address, bytes32){
        return (positions[index].token, positions[index].specifier);
    }

    function getPositionsIssued(uint index) returns(uint){
        bondage = BondageInterface(coord.getContract("BONDAGE")); 
        return bondage.getDotsIssued(address(this), positions[index].specifier);
    }

    function getPositionTotal(uint index) returns(uint){
        bondage = BondageInterface(coord.getContract("BONDAGE")); 
        return bondage.getBoundDots(address(this), address(this), positions[index].specifier);
    }

///UTILITIES    

    // https://ethereum.stackexchange.com/a/1154
    function bytes32ArrayToString (bytes32[] data) returns (string) {
        bytes memory bytesString = new bytes(data.length * 32);
        uint256 urlLength;
        for (uint256 i=0; i<data.length; i++) {
            for (uint256 j=0; j<32; j++) {
                byte char = byte(bytes32(uint(data[i]) * 2 ** (8 * j)));
                if (char != 0) {
                    bytesString[urlLength] = char;
                    urlLength += 1;
                }
            }
        }
        bytes memory bytesStringTrimmed = new bytes(urlLength);
        for (i=0; i<urlLength; i++) {
            bytesStringTrimmed[i] = bytesString[i];
        }

        return string(bytesStringTrimmed);
    }

    // https://ethereum.stackexchange.com/questions/9142/how-to-convert-a-string-to-bytes32
    function stringToBytes32(string memory source) returns (bytes32 result) {
        bytes memory tempEmptyStringTest = bytes(source);
        if (tempEmptyStringTest.length == 0) {
            return 0x0;
        }

        assembly {
            result := mload(add(source, 32))
        }
    }

}
