import "../zap/lib/platform/EthAdapter.sol";
import "../zap/lib/platform/TokenAdapter.sol";

/*
competiton is initialized with initializeCompetition and initializeGateway
users can then bond to EthGatedMarket gateway for gateway access tokens
users ( need not have gateway access tokens) can then create candidateMarkets with intializeMarketCurve
users holding gateway tokens can use them to bond to contest candidate curves
oracle can close the contest calling `close`
oracle can choose winner calling `judge`
once judged, holders of winning contest can call 
users holding tokens of winning candidate curve can call `redeem`. contract will then send winning token holders their proportional share of winnings

*/

contract PrizePool is EthAdapter {

    enum ContestStatus { 
        Uninitialized,    // oracles, curves unset 
        Initialized,      // ready for buys
        ReadyToSettle,    // ready for judgement 
        Judged,           // winner determined 
        Settled           // value of winning tokens determined 
    }

    struct Candidate {
        bytes32 proposal;
        address tokenAddress;
        address providerAddress;
    }

    //gateway 
    bool public bondAllow; // gateway open for bonding
    bool public unbondAllow; // gateway open for unbonding
    bytes32 public gatewaySpecifier; // identifier of 

    //contest
    string public title;      // title of the contest
    address public oracle;    // address of oracle who will choose the winner
    bytes32 public winner;    // curve identifier of the winner 
    uint256 public winValue;  // final value of the winning token

    ContestStatus status;
    mapping(bytes32 => Candidate) candidates; //candidate specifier -> candidate 
    bytes32[] candidateList;

    FactoryTokenInterface public gatewayToken;//token used to bond in gated markets
    TokenAdapter public marketFactory;//factory for gated curves

   /**
   * @dev Create a new PrizePool contract 
   * @param coordinator address address authority for zap contracts deployment 
   * @param tokenFactory address deployed token factory, able to mint tokens for user bonds 
   */
    constructor(address coordinator, address tokenFactory)
    EthAdapter(coordinator, tokenFactory, 1) {

        bondAllow = false;
        unbondAllow = false;
        status = ContestStatus.Uninitialized;         
    }

   /**
   * @dev Owner owner set address of gated market contract, which will spawn candidates backed by bonding curves 
   * @param market TokenAdapter market contract address 
   */
    function setMarket(TokenAdapter market) onlyOwner {
        marketFactory = market;
    }

   /**
   * @dev initialize title and oracle arbitrator fo competition 
   * @param _title string  
   * @param  _oracle address
   */
    function initializeCompetition (
        string _title, 
        address _oracle
    ) onlyOwner {

        title = _title;
        oracle = _oracle;

        if(gatewaySpecifier != bytes32(0)){
           status = ContestStatus.Initialized;         
        } 
    }

   /**
   * @dev initiallize gateway eth->gateway token curve, set exchange rate of eth/reserve token
   * @param title bytes32 hexencoded title of gateway. Can be url of rules for example
   * @param pubKey uint256 public key of contest deployment. Can be used for offchain private messending to deployerc 
   * @param specifier bytes32 bonding curve identifier( zap contracts)
   * @param symbol bytes32 hex encoded token symbol for gateway ERC20 token 
   * @param curve int256[] bonding curve encoding ( zap curve encoding as sum of powers. SEE http://tech.zap.org/docs/overview.html)  
   * @param adapterRate eth/reserve token ( zap contracts require single utility token for bonding, which happens under the hood)
   * @return address gateway ERC20 token address  
   */
    function initializeGateway( 
        bytes32 title, 
        uint256 pubKey,
        bytes32 specifier, 
        bytes32 symbol, 
        int256[] curve,
        uint256 adapterRate
        ) returns(address){
        
        require(gatewaySpecifier == bytes32(0));
        gatewayToken = FactoryTokenInterface(
            initializeCurve(
                pubKey, title, specifier, symbol, curve
            )
        );

        gatewaySpecifier = specifier;
        setAdapterRate(adapterRate);
        bondAllow = true;
        if(oracle != address(0)){
           status = ContestStatus.Initialized;         
        } 

        return gatewayToken;    
    }

   /* @dev bond to obtain gateway tokens in exchange for eth, able to bond to gated curves
   *  @param quantity uint number of gateway tokens user seeks to obtain by bonding ETH 
   */ 
   function gatewayBond(uint quantity) public payable {

        require(bondAllow, "bond not allowed");
        super.bond(msg.sender, gatewaySpecifier, quantity);
    }  

    /* @dev unbond to obtain eth in exchange for gateway tokens
       @param quantity uint number of gateway tokens user would like to burn for ETH
    */
   function gatewayUnbond(uint quantity) public {

        require(unbondAllow, "unbond not allowed");
        super.unbond(msg.sender, gatewaySpecifier, quantity);
    }

   /**
   * @dev initiallize candidate gateway-token->candidate-token curve. This is the solution people bond to
   * @param proposal bytes32 hexencoded candidate proposal of gateway. Can be url for example
   * @param pubKey uint256 public key of contest deployment. Can be used for offchain private messending to deployerc 
   * @param specifier bytes32 bonding curve identifier( zap contracts)
   * @param symbol bytes32 hex encoded token symbol for gateway ERC20 token 
   * @param curve int256[] bonding curve encoding ( zap curve encoding as sum of powers. SEE http://tech.zap.org/docs/overview.html)  
   * @return address candidate ERC20 token address  
   */
    function initializeMarketCurve(
        uint256 pubKey,
        bytes32 proposal, 
        bytes32 specifier, 
        bytes32 symbol, 
        int256[] curve
    ) public returns(address) {

        require(
            status == ContestStatus.Initialized, "not initialized"
        );
        
         
        candidates[specifier] =Candidate(
            proposal, 
            marketFactory.initializeCurve(    
                pubKey, proposal, specifier, symbol, curve
            ),
            msg.sender
        );
        candidateList.push(specifier);

        return candidates[specifier].tokenAddress; 
    }
    
   /* @dev bond to candidate curve market with gateway token
   *  @param quantity uint number of candidate tokens user seeks to obtain in exchange for bonded gateway token 
   *  @param specifier bytes32 identifier of the candidate curve 
   */ 
   function marketBond(bytes32 specifier, uint quantity) {

        marketFactory.ownerBond(msg.sender, specifier, quantity);
    }

   /* @dev bond to candidate curve market with gateway token
   *  @param quantity uint number of candidate tokens user seeks to obtain in exchange for bonded gateway token 
   *  @param specifier bytes32 identifier of the candidate curve 
   */ 
    function marketUnbond(bytes32 specifier, uint quantity) {

        marketFactory.ownerUnbond(msg.sender, specifier, quantity);
    }

    /* @dev end the contest
    */
    function close() onlyOwner {
       status = ContestStatus.ReadyToSettle; 
    }

    /* @dev oracle calls to assign winner via candidate identifier
    *  @param specifier bytes32 indentifier for winning candidate curve
    */
    function judge(bytes32 specifier) {
        require( status == ContestStatus.ReadyToSettle, "not closed" );
        require( msg.sender == oracle, "not oracle");
        winner = specifier;
        status = ContestStatus.Judged;
    }

    /* @dev calculate value of winning candidate token
    */
    function settle() {
        require( status == ContestStatus.Judged, "winner not determined");
        
        uint numWin =  BondageInterface( 
            coord.getContract("BONDAGE")).getDotsIssued(marketFactory, winner);  

        winValue = address(this).balance / numWin;
        status = ContestStatus.Settled;
    }

    /* @dev call to redeem user balance of winning token for respective ETH value
    */
    function redeem() {
        require(status == ContestStatus.Settled, "contest not settled");        
        uint reward = winValue * FactoryTokenInterface(candidates[winner].tokenAddress).balanceOf(msg.sender);
        msg.sender.transfer(reward);
    }

    ///allow bond
    function allowBond(bool _allow) onlyOwner {
        bondAllow = _allow;
    }

    ///allow unbond
    function allowUnbond(bool _allow) onlyOwner {
        unbondAllow = _allow;
    }

    /* @dev return list of all candidate curve identifiers
    */
    function getCandidates() view returns(bytes32[]) {
        return candidateList;
    }

    /* @dev return proposal given candidate identifier 
       @param specifier bytes32 candidate curve identifier
    */
    function getCandidatesProposal(bytes32 specifier) view returns(bytes32) {
        return candidates[specifier].proposal;
    }

    /* @dev return status of contest
    */
    function getStatus() view returns(ContestStatus) {
        return ContestStatus(status);
    }

    /* @dev return contest title 
    */
    function getContesTitle() view returns(string) {
        return title;
    }

    /* @dev return contest rules 
    */
    function getContesRules() view returns(bytes32) {
        return RegistryInterface( 
            coord.getContract("REGISTRY")).getProviderTitle(address(this));  
    }

    /* @dev return oracle address 
    */
    function getOracle() view returns(address) {
        return oracle;
    }

    /* @dev return winner address 
    */
    function getWinner() view returns(bytes32) {
        return winner;
    }

    /* @dev return value of winning token */
    function getWinValue() view returns(uint256) {
        return winValue;
    }

    /* @dev return total value of winning tokenvalue of winning token 
    */
    function getWinnerIssuance() view returns(uint256) {
        return BondageInterface( 
            coord.getContract("BONDAGE")).getDotsIssued(marketFactory, winner);  
    }

    /* @dev return address of candidate token
       @param specifier bytes32 candidate curve specifier
    */
    function getCandidateToken(bytes32 specifier) view returns(address) {
        return FactoryTokenInterface(candidates[specifier].tokenAddress);
    }

    /* @dev return user balance of a candidate tokens
       @param specifier bytes32 candidate curve specifier
    */
    function getCandidateBalance(bytes32 specifier) view returns(uint256) {
        return FactoryTokenInterface(candidates[specifier].tokenAddress).balanceOf(msg.sender);
    }
}
